# taptempo-v

Implémentation de [taptempo](https://taptempo.tuxfamily.org/) en [V](https://vlang.io).

Voir la [liste des toutes les implémentations existantes de TapTempo](https://linuxfr.org/wiki/taptempo).

## Prérequis

Avoir V d'[installer sur sa machine](https://github.com/vlang/v/blob/master/README.md#installing-v-from-source).

## Note

Testé uniquement sous Linux.

## Lancement

`v run taptempo.v`