import os
import readline
import time

const (
	sample_size = 5
)

struct TapTempo {
mut:
	samples           [sample_size]time.Duration
	current_hit_count u8
}

fn (mut t TapTempo) get_tempo(delta_t time.Duration) ?int {
	t.samples[t.current_hit_count] = delta_t
	t.current_hit_count++

	if t.current_hit_count >= sample_size && 0 !in t.samples {
		t.current_hit_count = 0
		mut sum := time.Duration(0)
		for s in t.samples {
			sum += s
		}
		mean := time.Duration(sum / sample_size)
		return int(60 / mean.seconds())
	} else {
		return none
	}
}

fn main() {
	println('Bienvenue dans TapTempo !')
	println('Appuyer sur une touche en cadence pour mesurer le tempo (q pour quitter).')

	mut r := readline.Readline{}
	r.enable_raw_mode()
	defer {
		r.disable_raw_mode()
	}

	stdin := os.stdin()
	mut buffer := []u8{len: 1, cap: 1}
	mut previous_time := time.now()
	mut taptempo := TapTempo{}

	_ := stdin.read(mut buffer)!
	for buffer.bytestr() != 'q' {
		_ := stdin.read(mut buffer)!
		if tempo := taptempo.get_tempo(time.since(previous_time)) {
			println('Tempo : ${tempo} bpm')
		}
		previous_time = time.now()
	}
	println('Au revoir !')
}
